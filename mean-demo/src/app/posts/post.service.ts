import { Post } from './post.model';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({providedIn: 'root'})
export class PostService {
  private posts: Post[] = [];
  private postsUpdated = new Subject<Post[]>();

  getPost() {
    return [...this.posts];
  }

  getPostUpdateListener(){
    return this.postsUpdated.asObservable();
  }

  addPost(titlePost: string, contentPost: string) {
    const post: Post = { title: titlePost, content: contentPost };
    this.posts.push(post);
    this.postsUpdated.next([...this.posts]);
    }
  }
