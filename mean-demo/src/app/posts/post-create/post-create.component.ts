import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PostService } from '../post.service';

@Component({
  selector: 'app-create-post',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreatComponent {
  constructor(public postPervice: PostService) { }

  onAddPost(form: NgForm) {
    if (form.invalid) {
      return;
    }
    this.postPervice.addPost(form.value.title, form.value.content);
    form.resetForm();
  }
}
